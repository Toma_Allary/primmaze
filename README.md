Jeux fait dans le cadre du travail #1 du cours INF1008 du programme 
informatique de l'Université du Québec à Trois-Rivières.  

Vous jouez Timmy et vous devez trouver la sortie d'un labyrinthe fait 
avec l'algorithme de Prim (2D) ou fait avec du backtracking (3D).  

---  



Games done as part of work # 1 of course INF1008 of the computer 
science program of University of Quebec at Trois-Rivières.  

You play Timmy and you have to find the exit of a maze made with 
Prim's algorithm (2D) or made with backtracking (3D).  


---
Made by Benoit Matteau and Toma Allary.  

###### To dowload:  
* Go to PrimMaze/Build/Build.zip and click on download raw.  Unzip on your pc and you will find the PrimMaze.exe